package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AuthorizationPage extends Page {
	public AuthorizationPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(how = How.XPATH, using = ".//*[@id='Email']")
	public WebElement loginField;

	@FindBy(how = How.XPATH, using = ".//*[@id='next']")
	public WebElement nextButton;

	@FindBy(how = How.XPATH, using = ".//*[@id='errormsg_0_Passwd']")
	public WebElement errorMessage;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='errormsg_0_Email']")
	public WebElement errorMessageLogin;

	@FindBy(how = How.XPATH, using = ".//*[@id='PersistentCookie']")
	public WebElement stateCheckbox;

	@FindBy(how = How.XPATH, using = ".//*[@id='Passwd']")
	public WebElement passwordField;

	@FindBy(how = How.XPATH, using = ".//*[@id='signIn']")
	public WebElement signUpButton;

	public AuthorizationPage sendKeysLoginField(String login) {
		loginField.sendKeys(login);
		return PageFactory.initElements(webDriver, AuthorizationPage.class);
	}

	public AuthorizationPage clickNextButton() {
		nextButton.click();
		return PageFactory.initElements(webDriver, AuthorizationPage.class);
	}

	public AuthorizationPage clickOnCheckbox() {
		stateCheckbox.click();
		return PageFactory.initElements(webDriver, AuthorizationPage.class);
	}

	public AuthorizationPage sendKeysPasswordField(String password) {
		passwordField.sendKeys(password);
		return PageFactory.initElements(webDriver, AuthorizationPage.class);
	}

	public MainPage clickSignInButton() {
		signUpButton.click();
		return PageFactory.initElements(webDriver, MainPage.class);
	}
	
	public AuthorizationPage textContent() {
		JavascriptExecutor js = (JavascriptExecutor) webDriver;
		js.executeScript("document.getElementById('errormsg_0_Email').innerHTML = document.getElementById('errormsg_0_Email').textContent");		
		return PageFactory.initElements(webDriver, AuthorizationPage.class);
	}
}

