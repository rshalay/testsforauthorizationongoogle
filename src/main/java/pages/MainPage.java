package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends Page {
	public MainPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(how = How.XPATH, using = ".//*[@id='gb_70']")
	public WebElement signIn;
	
	@FindBy(how = How.XPATH, using = ".//*[contains(@class,'gb_b gb_8a gb_R')]")
	public WebElement authorizationIcon;

	@FindBy(how = How.XPATH, using = ".//*[@id='signout']")
	public WebElement signOutButtonFireFox;

	@FindBy(how = How.XPATH, using = ".//*[@id='gb_71']")
	public WebElement signOutButtonChrome;

	@FindBy(how = How.CSS, using = "#gb_70")
	public WebElement signUpButton;

	public MainPage clickLogoutButton(String browserName) {
		if (browserName.equals("firefox")) {
			signOutButtonFireFox.click();
		} else {
			signOutButtonChrome.click();
		}
		return PageFactory.initElements(webDriver, MainPage.class);
	}

	public MainPage clickAuthorizationIcon() {
		authorizationIcon.click();
		return PageFactory.initElements(webDriver, MainPage.class);
	}

	public AuthorizationPage clickOnButtonSignIn() {
		signIn.click();
		return PageFactory.initElements(webDriver, AuthorizationPage.class);
	}
}
