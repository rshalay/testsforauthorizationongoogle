package utility;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


public class HtmlParser {
	public CloseableHttpClient httpClient;
	public HttpGet httpGet;
	public CloseableHttpResponse response;
	public HttpResponse httpResponse;


	public void htmlParser(String testUrl) {	
		httpClient = HttpClients.createDefault();
		httpGet = new HttpGet(testUrl);
		

		try {
			response = httpClient.execute(httpGet);
		} catch (ClientProtocolException e1) {

			e1.printStackTrace();
		} catch (IOException e1) {

			e1.printStackTrace();
		}
		try {
			httpResponse = httpClient.execute(httpGet);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}