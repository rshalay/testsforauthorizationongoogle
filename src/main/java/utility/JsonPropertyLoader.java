package utility;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonPropertyLoader {

	public static String jsonPropertyLoader(String propertyName) {
		String props = null;
		JSONParser parser = new JSONParser();

		try {

			Object obj = parser.parse(new FileReader("src/main/resources/JSONproperties.json"));

			JSONObject jsonObject = (JSONObject) obj;

			props = (String) jsonObject.get(propertyName);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return props;
	}

	public static Object[][] jsonPropertyLoaderArray() {
		Object[][] object = new Object[3][2];
		JSONParser parser = new JSONParser();		
		try {
			Object obj = parser.parse(new FileReader("src/main/resources/JSONproperties.json"));
			JSONObject jsonObject = (JSONObject) obj;		
			
			JSONArray lArray = (JSONArray) jsonObject.get("loginArray");
			Iterator<String> iteratorLogin = lArray.iterator();

			JSONArray pArray = (JSONArray) jsonObject.get("passwordArray");
			Iterator<String> iteratorPassword = pArray.iterator();	
		
			int i=0;
			while (iteratorLogin.hasNext() && iteratorPassword.hasNext()) {
				object [i][0]  = iteratorLogin.next();
				object [i][1]  = iteratorPassword.next();
				i++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return object;
	}
}