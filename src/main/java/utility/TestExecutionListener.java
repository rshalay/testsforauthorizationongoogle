package utility;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import ru.yandex.qatools.allure.annotations.Attachment;
import utility.WebDriverFactory;


public class TestExecutionListener extends TestListenerAdapter {

	@Override
	public void onTestStart(ITestResult result) {
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		makeScreenshot();
		if (result.getThrowable() != null) {
			result.getThrowable().printStackTrace();
		}
	}

	@Attachment(value = "Page screenshot", type = "image/png")
	private byte[] makeScreenshot() {
		return ((TakesScreenshot) WebDriverFactory.getSetDriver()).getScreenshotAs(OutputType.BYTES);
	}
}
