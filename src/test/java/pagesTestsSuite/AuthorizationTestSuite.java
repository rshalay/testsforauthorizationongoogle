package pagesTestsSuite;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class AuthorizationTestSuite extends TestBase {

	@Test(groups = "positive")
	@Parameters({ "browserName" })
	public void loginTest(String browserName) {
		authorizationPage = mainPage.clickOnButtonSignIn();
		authorizationPage = authorizationPage.sendKeysLoginField(loginIsCorrect);
		authorizationPage = authorizationPage.clickNextButton();
		authorizationPage = authorizationPage.sendKeysPasswordField(passwordIsCorrect);
		mainPage = authorizationPage.clickSignInButton();
		Assert.assertTrue(mainPage.authorizationIcon.getAttribute("title").contains(loginIsCorrect));
	}

	@Test(groups = "positive")
	@Parameters({ "browserName" })
	public void logoutTest(String browserName) {
		authorizationPage = mainPage.clickOnButtonSignIn();
		authorizationPage = authorizationPage.sendKeysLoginField(loginIsCorrect);
		authorizationPage = authorizationPage.clickNextButton();
		authorizationPage = authorizationPage.sendKeysPasswordField(passwordIsCorrect);
		mainPage = authorizationPage.clickSignInButton();
		mainPage = mainPage.clickAuthorizationIcon();
		mainPage = mainPage.clickLogoutButton(browserName);
		Assert.assertEquals("gb_70", mainPage.signUpButton.getAttribute("id"));
	}

	@Test(groups = "negativ", dataProvider = "loginAndPassword")
	public void loginInWithIncorrectValues(String login, String password) {
		authorizationPage = mainPage.clickOnButtonSignIn();
		authorizationPage = authorizationPage.sendKeysLoginField(login);
		authorizationPage = authorizationPage.clickNextButton();
		authorizationPage = authorizationPage.sendKeysPasswordField(password);
		mainPage = authorizationPage.clickSignInButton();
		Assert.assertEquals("rgba(221, 75, 57, 1)", authorizationPage.errorMessage.getCssValue("border-top-color"));
		Assert.assertEquals("rgba(221, 75, 57, 1)", authorizationPage.errorMessage.getCssValue("border-right-color"));
		Assert.assertEquals("rgba(221, 75, 57, 1)", authorizationPage.errorMessage.getCssValue("border-bottom-color"));
		Assert.assertEquals("rgba(221, 75, 57, 1)", authorizationPage.errorMessage.getCssValue("border-left-color"));
		Assert.assertEquals("The email and password you entered don't match.",
				authorizationPage.errorMessage.getText());
	}

	@Test(groups = "negativ")
	public void loginIsEmpty() {
		authorizationPage = mainPage.clickOnButtonSignIn();
		authorizationPage = authorizationPage.sendKeysLoginField("");
		authorizationPage = authorizationPage.clickNextButton();
		Assert.assertEquals("Please enter your email. ", authorizationPage.errorMessageLogin.getText());
	}

	@Test(groups = "negativ")
	public void PassvordIsEmpty() {
		authorizationPage = mainPage.clickOnButtonSignIn();
		authorizationPage = authorizationPage.sendKeysLoginField(loginIsCorrect);
		authorizationPage = authorizationPage.clickNextButton();
		authorizationPage = authorizationPage.sendKeysPasswordField("");
		mainPage = authorizationPage.clickSignInButton();
		Assert.assertEquals("Please enter your password. ", authorizationPage.errorMessage.getText());
	}

	@Test(groups = "negativ")
	public void LoginInWithIncorrectEmail() {
		authorizationPage = mainPage.clickOnButtonSignIn();
		authorizationPage = authorizationPage.sendKeysLoginField("emailsfg@sd.sd");
		authorizationPage = authorizationPage.clickNextButton();
		authorizationPage = authorizationPage.textContent();
		Assert.assertEquals("Sorry, Google doesn't recognize that email. Create an account using that address?",
				authorizationPage.errorMessageLogin.getText());
	}
}
