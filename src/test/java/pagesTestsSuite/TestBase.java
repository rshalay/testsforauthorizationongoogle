package pagesTestsSuite;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;

import pages.AuthorizationPage;
import pages.MainPage;
//import pages.ResaltOfAuthorization;
import utility.TestExecutionListener;
import utility.HtmlParser;
import utility.JsonPropertyLoader;
import utility.WebDriverFactory;
import utility.XmlPropertyLoader;

@Listeners({ TestExecutionListener.class })
public abstract class TestBase {
	protected WebDriver webDriver;
	protected HtmlParser htmlStatus;
	protected String testUrl;
	protected String timeout;
	protected String loginIsCorrect;
	protected String passwordIsCorrect;
	protected MainPage mainPage;
	protected AuthorizationPage authorizationPage;

	@BeforeMethod
	@Parameters({ "browserName" })
	public void setup(String browserName) throws Exception {
		testUrl = XmlPropertyLoader.loadProperty("testsite.url");
		timeout = XmlPropertyLoader.loadProperty("implicit-timeout");
		loginIsCorrect = JsonPropertyLoader.jsonPropertyLoader("loginIsCorrect");
		passwordIsCorrect = JsonPropertyLoader.jsonPropertyLoader("passwordIsCorrect");
		webDriver = WebDriverFactory.getInstance(browserName);
		webDriver.manage().window().maximize();
		webDriver.manage().timeouts().implicitlyWait(Integer.valueOf(timeout), TimeUnit.SECONDS);
		webDriver.manage().timeouts().pageLoadTimeout(Integer.valueOf(timeout), TimeUnit.SECONDS);
		webDriver.get(testUrl);
		mainPage = PageFactory.initElements(webDriver, MainPage.class);
		authorizationPage = PageFactory.initElements(webDriver, AuthorizationPage.class);

		htmlStatus = new HtmlParser();
		htmlStatus.htmlParser(testUrl);
	}

	@DataProvider(name = "tick")
	public Object[] tick() {
		return new Object[] { "checkOut", "checkIn" };
	}

	
	@DataProvider(name = "loginAndPassword")
	public Object[][] loginAndPassword() {
		return JsonPropertyLoader.jsonPropertyLoaderArray();
	}

	@AfterMethod
	public void tearDown() throws Exception {
		if (htmlStatus.response.getStatusLine() != null) {
			WebDriverFactory.killDriverInstance();
		} else if (webDriver != null) {
			WebDriverFactory.killDriverInstance();
		}
	}
}
